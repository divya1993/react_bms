/* eslint-disable */
import React from 'react';
import { shallow } from 'enzyme';
import { LoginPage } from '../src/LoginPage';
import configureStore from 'redux-mock-store';

describe('Shallow rendered login Page', () => {
    const initialState = {
		 username: 'aaa',
		 password: 'aaa',
		 submitted: true,
        authentication: {
            user: {}
        }
    }
    const mockStore = configureStore();
    let store, enzymeWrapper;

    beforeEach(() => {
        store = mockStore(initialState)
        enzymeWrapper = shallow(<LoginPage store={store} />);
    })

    it('+++ should render Login page', () => {
        expect(enzymeWrapper).toMatchSnapshot();

    });


});
