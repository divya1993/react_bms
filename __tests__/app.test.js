/* eslint-disable */
import React from 'react';
import { shallow } from 'enzyme';
import { App } from '../src/App';
import configureStore from 'redux-mock-store';

describe('Shallow rendered App', () => {
    const initialState = {
		 alert:{
        	type: 'SUCCESS',
        	message:'success'
        }
    }
    const mockStore = configureStore();
    let store, enzymeWrapper;

    beforeEach(() => {
        store = mockStore(initialState)
        enzymeWrapper = shallow(<App store={store} />);
    })

    it('+++ should render App', () => {
        expect(enzymeWrapper).toMatchSnapshot();

    });


});
