/* eslint-disable */
import React from 'react';
import { shallow } from 'enzyme';
import { LoanPage } from '../src/LoanPage';
import configureStore from 'redux-mock-store';

describe('Shallow rendered Personal Loan Page', () => {
    const initialState = {
    		loanUser: {
                loanType: 'Personal',
				loanAmount: '100000',
				loanApplyDate: new Date(),
				rateOfInterest: '15',
				durationOfTheLoan: '5',
				showEducationalDetails: false,
				showOtherDetails: true,
				courseFee: '',
				course: '',
				fatherName: '',
				fatherOccupation: '',
				annualIncome: '',
				companyName: 'sda',
				designation: 'sad',
				totalExp: '5',
				currentExp: '3',
				show:false,
				message:'',
				style:'',
				completeFlag:true
            },
            submitted: true,
            authentication: {
                user: {}
            }
    }
    const mockStore = configureStore();
    let store, enzymeWrapper;

    beforeEach(() => {
        store = mockStore(initialState)
        enzymeWrapper = shallow(<LoanPage store={store} />);
    })

    it('+++ should render Loan page', () => {
        expect(enzymeWrapper).toMatchSnapshot();

    });

});

    describe('Shallow rendered Education Loan Page', () => {
        const initialState = {
        		loanUser: {
                    loanType: 'Education',
    				loanAmount: '100000',
    				loanApplyDate: new Date(),
    				rateOfInterest: '9',
    				durationOfTheLoan: '5',
    				showEducationalDetails: true,
    				showOtherDetails: false,
    				courseFee: '100000',
    				course: 'sdas',
    				fatherName: 'asds',
    				fatherOccupation: 'asds',
    				annualIncome: '3000000',
    				companyName: '',
    				designation: '',
    				totalExp: '',
    				currentExp: '',
    				show:false,
    				message:'',
    				style:'',
    				completeFlag:true
                },
                submitted: true,
                authentication: {
                    user: {}
                }
        }
        const mockStore = configureStore();
        let store, enzymeWrapper;

        beforeEach(() => {
            store = mockStore(initialState)
            enzymeWrapper = shallow(<LoanPage store={store} />);
        })

        it('+++ should render Loan page', () => {
            expect(enzymeWrapper).toMatchSnapshot();

        });

    });