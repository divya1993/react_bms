/* eslint-disable */
import React from 'react';
import { shallow } from 'enzyme';
import { DepositPage } from '../src/DepositPage';
import configureStore from 'redux-mock-store';

describe('Shallow rendered Deposit Page', () => {
    const initialState = {
		user: {
            accountType: 'Savings',
			depositAmount: '1000',
			availableBalance: '6000',
			message:'Deposited successfully!',
			show:true,
			style:'alert alert-success'
        },
        submitted: false,
        authentication: {
            user: {}
        }
    }
    const mockStore = configureStore();
    let store, enzymeWrapper;

    beforeEach(() => {
        store = mockStore(initialState)
        enzymeWrapper = shallow(<DepositPage store={store} />);
    })

    it('+++ should render Deposit page', () => {
        expect(enzymeWrapper).toMatchSnapshot();

    });


});

describe('Shallow rendered Deposit Page for salary acc', () => {
    const initialState = {
		user: {
            accountType: 'Salary',
			depositAmount: '1000',
			availableBalance: '1000',
			message:'Deposited successfully!',
			show:true,
			style:'alert alert-success'
        },
        submitted: false,
        authentication: {
            user: {}
        }
    }
    const mockStore = configureStore();
    let store, enzymeWrapper;

    beforeEach(() => {
        store = mockStore(initialState)
        enzymeWrapper = shallow(<DepositPage store={store} />);
    })

    it('+++ should render Deposit page', () => {
        expect(enzymeWrapper).toMatchSnapshot();

    });


});
describe('Shallow rendered error Deposit Page for salary acc', () => {
    const initialState = {
		user: {
            accountType: 'Salary',
			depositAmount: '',
			availableBalance: '',
			message:'Please enter required details!',
			show:true,
			style:'alert alert-danger'
        },
        submitted: true,
        authentication: {
            user: {}
        }
    }
    const mockStore = configureStore();
    let store, enzymeWrapper;

    beforeEach(() => {
        store = mockStore(initialState)
        enzymeWrapper = shallow(<DepositPage store={store} />);
    })

    it('+++ should render Deposit page', () => {
        expect(enzymeWrapper).toMatchSnapshot();

    });


});