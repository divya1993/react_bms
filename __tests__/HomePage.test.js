/* eslint-disable */
import React from 'react';
import { shallow } from 'enzyme';
import { HomePage } from '../src/HomePage';
import configureStore from 'redux-mock-store';

describe('Shallow rendered Home Page', () => {
    const initialState = {
        user: [{
        	username: 'aaa',
        }],
        authentication: {
            user: {}
        }
    }
    const mockStore = configureStore();
    let store, enzymeWrapper;

    beforeEach(() => {
        store = mockStore(initialState)
        enzymeWrapper = shallow(<HomePage store={store} />);
    })

    it('+++ should render Home page', () => {
        expect(enzymeWrapper).toMatchSnapshot();

    });


});
