/* eslint-disable */
import React from 'react';
import { shallow } from 'enzyme';
import { RegisterPage } from '../src/RegisterPage';
import configureStore from 'redux-mock-store';

describe('Shallow rendered Register Page', () => {
    const initialState = {
		 submitted: true,
        authentication: {
            user: {
            	name: 'aaa',
                username: 'aaa',
                password: 'aaa',
				address: 'abc',
				state: 'TN',
				country: 'INDIA',
				emailAddress: 'abc@abc.com',				
				contactNo: '1234567890',
				dateOfBirth: new Date(),				
				accountType: 'Savings',
				branchName: 'CHN',				
				initialDepositAmount: '1000',
				identificationProofType: 'abc',
				identificationDocumentNo: '123',
				age: '20',
				citizenStatus: 'Normal'
					}
        }, registration:{
        	registering: true
        }
    }
    const mockStore = configureStore();
    let store, enzymeWrapper;

    beforeEach(() => {
        store = mockStore(initialState)
        enzymeWrapper = shallow(<RegisterPage store={store} />);
    })

    it('+++ should render Register page', () => {
        expect(enzymeWrapper).toMatchSnapshot();

    });


});
