/* eslint-disable */
import configureMockStore from 'redux-mock-store'

// In a real application we would import this from our actions
const createSuccess = (user) => ({
  type: 'LOGIN_SUCCESS',
  user
});

const createFailure = (user) => ({
	  type: 'LOGIN_FAILURE',
	  user
	});

// Create a mock store
const mockStore = configureMockStore()
const store = mockStore({})
describe('action creators', () => {
  it('creates LOGIN_SUCCESS when creating a to-do was successful', () => {
    // Dispatch the createSuccess action with the values of a new to-do.
    store.dispatch(createSuccess(
      {
        
        	"loggedIn": true,
            "user": {}
      }
    ));
    expect(store.getActions()).toMatchSnapshot();
  });
});

describe('action creators', () => {
	  it('creates LOGIN_FAILURE when creating a to-do failed', () => {
	    store.dispatch(createFailure(
	      {}
	    ));
	    expect(store.getActions()).toMatchSnapshot();
	  });
	});

const registerSuccess = (user) => ({
	  type: 'REGISTER_REQUEST',
	  user
	});

describe('action creators', () => {
	  it('REGISTER_REQUEST when registering a to-do was successful', () => {
	    store.dispatch(registerSuccess(
	      {
	    	  "registering": true
	      }
	    ));
	    expect(store.getActions()).toMatchSnapshot();
	  });
	});

