import React from 'react';
import { Link } from 'react-router-dom';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker-cssmodules.css';
import { connect } from 'react-redux';
import { userActions } from '../_actions';

class LoanPage extends React.Component{
	
	componentDidMount() {
        this.props.getUsers();
    }
	
	constructor(props) {
        super(props);
        this.state = {
            loanUser: {
                loanType: '',
				loanAmount: '',
				loanApplyDate: new Date(),
				rateOfInterest: '',
				durationOfTheLoan: '5',
				showEducationalDetails: false,
				showOtherDetails: false,
				courseFee: '',
				course: '',
				fatherName: '',
				fatherOccupation: '',
				annualIncome: '',
				companyName: '',
				designation: '',
				totalExp: '',
				currentExp: '',
				show:'',
				message:'',
				style:'',
				completeFlag:false
            },
            submitted: false
        };
		this.handleChange = this.handleChange.bind(this);
		this.handleDateChange = this.handleDateChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
		this.handleLoanTypeChange = this.handleLoanTypeChange.bind(this);
    }

	handleChange(event) {
        const { name, value } = event.target;
        const { loanUser } = this.state;
        this.setState({
            loanUser: {
                ...loanUser,
                [name]: value
            }
        });
    }
	
	handleLoanTypeChange(event) {
        const { name, value } = event.target;
        const { loanUser } = this.state;
		console.log("value.."+value)
		var rateOfInterest = '15';
		var showEducationalDetails;
		var showOtherDetails;
		if(value === 'Personal'){
			rateOfInterest='15';
			showOtherDetails = true
			showEducationalDetails = false
		}
		if(value === 'Housing'){
			rateOfInterest='7';
			showOtherDetails = true
			showEducationalDetails = false
		}
		if(value === 'Education'){
			rateOfInterest='9';
			showOtherDetails = false
			showEducationalDetails = true
		}
		console.log("rateOfInterest.."+rateOfInterest)
        this.setState({
            loanUser: {
                ...loanUser,
                [name]: value,
				rateOfInterest: rateOfInterest,
				showEducationalDetails: showEducationalDetails,
				showOtherDetails: showOtherDetails,
				courseFee: '',
				course: '',
				fatherName: '',
				annualIncome: '',
				companyName: '',
				designation: '',
				totalExp: '',
				currentExp: ''
            }
        });
    }

	handleDateChange(date) {
        console.log("date "+date)
        const { loanUser } = this.state;
        this.setState({
            loanUser: {
                ...loanUser,
                loanApplyDate: date
            }
        });
    }
	
	handleSubmit(event) {
        event.preventDefault();

        this.setState({ submitted: true });
        const { loanUser } = this.state;
        if(loanUser.completeFlag){

		this.setState({
            loanUser: {
                ...loanUser,
				message: 'Loan submitted successfully!',
				style: 'alert alert-success',
				show: true
            },
			submitted: true
        });
	}else{
		this.setState({
            loanUser: {
                ...loanUser,
				message: 'Please enter required details!',
				style: 'alert alert-danger',
				show: true
            }
	});
	}
        
    }
	
	render() {
		const { registering  } = this.props;
        const { loanUser, submitted } = this.state;
		if(loanUser.loanType && loanUser.loanAmount && loanUser.loanApplyDate && loanUser.rateOfInterest && loanUser.durationOfTheLoan ){
			if((loanUser.annualIncome && loanUser.companyName && loanUser.designation && loanUser.totalExp && loanUser.currentExp)||
			(loanUser.courseFee && loanUser.course && loanUser.fatherName && loanUser.fatherOccupation && loanUser.annualIncome)){
				loanUser.completeFlag = true;
			}
		}
		
		return (
            <div >
				<div className="shadow-lg p-3 mb-5 bg-white rounded">
				<h1 align="center">Apply Loan</h1>
				</div>
				&nbsp; 
				{loanUser.show &&
                            <div className={loanUser.style} role="alert">{loanUser.message}</div>
                        }
				&nbsp; 
				<div className="col-md-6 col-md-offset-3">
				<form name="form" onSubmit={this.handleSubmit}>
						<div className={'form-group' + (submitted && !loanUser.loanType ? ' has-error' : '')}>                    
						<label className="font-italic" htmlFor="loanType">Loan Type</label>
						<div>                        
						<input type="radio" className="form-check-input" name="loanType" value="Personal" onChange={this.handleLoanTypeChange} /> 
						<label className="form-check-label" htmlFor="accountType">
						    Personal
						  </label>
						</div>
						<div>                        
						<input type="radio" className="form-check-input" name="loanType" value="Housing" onChange={this.handleLoanTypeChange} /> 
						<label className="form-check-label" htmlFor="accountType">
						    Housing
						  </label>
						</div>
						<div>                        
						<input type="radio" className="form-check-input" name="loanType" value="Education" onChange={this.handleLoanTypeChange} /> 
						<label className="form-check-label" htmlFor="accountType">
						    Education
						  </label>
						</div>	
                        {submitted && !loanUser.loanType &&
                            <div className="help-block">Loan Type is required</div>
                        }
                    </div>
					<div className={'form-group' + (submitted && !loanUser.loanAmount ? ' has-error' : '')}>
                        <label className="font-italic" htmlFor="loanAmount">Loan Amount</label>
                        <input type="text" className="form-control" name="loanAmount" value={loanUser.loanAmount} onChange={this.handleChange} />
                        {submitted && !loanUser.loanAmount &&
                            <div className="help-block">Loan Amount is required</div>
                        }
						{submitted && !loanUser.loanAmount.match(/^[1-9]\d*$/) &&
                            <div className="help-block">Loan Amount should be greater than 0!</div>
                        }
                    </div>
					<div className={'form-group' + (submitted && !loanUser.loanApplyDate ? ' has-error' : '')}>
                        <label className="font-italic" htmlFor="loanApplyDate">Loan Apply Date</label>&nbsp;
                        <DatePicker className="form-control" name="loanApplyDate" selected={loanUser.loanApplyDate} onChange={this.handleDateChange} minDate={new Date()} />
                        {submitted && !loanUser.loanApplyDate &&
                            <div className="help-block">loan Apply Date is required</div>
                        }
                    </div>
					<div className={'form-group' + (submitted && !loanUser.rateOfInterest ? ' has-error' : '')}>
                        <label className="font-italic" htmlFor="rateOfInterest">Rate Of Interest</label>
                        <input type="text" className="form-control" name="rateOfInterest" value={loanUser.rateOfInterest} readOnly />
                        
                    </div>
					<div className={'form-group' + (submitted && !loanUser.durationOfTheLoan ? ' has-error' : '')}>                   
						<label className="font-italic" htmlFor="name">Duration Of The Loan (in years)</label>
						<div>                        
						<input type="radio" className="form-check-input" name="durationOfTheLoan" value="5" defaultChecked onChange={this.handleChange} /> 
						<label className="form-check-label" htmlFor="accountType">
						    5
						  </label>
						</div>
						<div>                        
						<input type="radio" className="form-check-input" name="durationOfTheLoan" value="10" onChange={this.handleChange} /> 
						<label className="form-check-label" htmlFor="accountType">
						    10
						  </label>
						</div>
						<div>                        
						<input type="radio" className="form-check-input" name="durationOfTheLoan" value="15" onChange={this.handleChange} /> 
						<label className="form-check-label" htmlFor="accountType">
						    15
						  </label>
						</div>	
						<div>                        
						<input type="radio" className="form-check-input" name="durationOfTheLoan" value="20" onChange={this.handleChange} /> 
						<label className="form-check-label" htmlFor="accountType">
						    20
						  </label>
						</div>	
                        {submitted && !loanUser.durationOfTheLoan &&
                            <div className="help-block">Duration Of The Loan is required</div>
                        }
                    </div>
					{loanUser.showEducationalDetails ?
					<div>
						<div className={'form-group' + (submitted && !loanUser.courseFee ? ' has-error' : '')}>
	                        <label className="font-italic" htmlFor="courseFee">Course Fee</label>
	                        <input type="text" className="form-control" name="courseFee" value={loanUser.courseFee} onChange={this.handleChange} />
	                        {submitted && !loanUser.courseFee &&
	                            <div className="help-block">Course Fee is required</div>
	                        }
	                    </div>
						<div className={'form-group' + (submitted && !loanUser.course ? ' has-error' : '')}>
	                        <label className="font-italic" htmlFor="course">Course</label>
	                        <input type="text" className="form-control" name="course" value={loanUser.course} onChange={this.handleChange} />
	                        {submitted && !loanUser.course &&
	                            <div className="help-block">Course is required</div>
	                        }
	                    </div>
						<div className={'form-group' + (submitted && !loanUser.fatherName ? ' has-error' : '')}>
	                        <label className="font-italic" htmlFor="fatherName">Father's Name</label>
	                        <input type="text" className="form-control" name="fatherName" value={loanUser.fatherName} onChange={this.handleChange} />
	                        {submitted && !loanUser.fatherName &&
	                            <div className="help-block">Father Name is required</div>
	                        }
	                    </div>
						<div className={'form-group' + (submitted && !loanUser.fatherOccupation ? ' has-error' : '')}>
	                        <label className="font-italic" htmlFor="fatherOccupation">Father's Occupation</label>
	                        <input type="text" className="form-control" name="fatherOccupation" value={loanUser.fatherOccupation} onChange={this.handleChange} />
	                        {submitted && !loanUser.fatherOccupation &&
	                            <div className="help-block">Father Occupation is required</div>
	                        }
	                    </div>
						<div className={'form-group' + (submitted && !loanUser.annualIncome ? ' has-error' : '')}>
	                        <label className="font-italic" htmlFor="annualIncome">Annual Income</label>
	                        <input type="text" className="form-control" name="annualIncome" value={loanUser.annualIncome} onChange={this.handleChange} />
	                        {submitted && !loanUser.annualIncome &&
	                            <div className="help-block">Annual Income is required</div>
	                        }
	                    </div>
					</div> :
					null
					}
					{loanUser.showOtherDetails ?
					<div>
						<div className={'form-group' + (submitted && !loanUser.annualIncome ? ' has-error' : '')}>
	                        <label className="font-italic" htmlFor="annualIncome">Annual Income</label>
	                        <input type="text" className="form-control" name="annualIncome" value={loanUser.annualIncome} onChange={this.handleChange} />
	                        {submitted && !loanUser.annualIncome &&
	                            <div className="help-block">Annual Income is required</div>
	                        }
	                    </div>
						<div className={'form-group' + (submitted && !loanUser.companyName ? ' has-error' : '')}>
	                        <label className="font-italic" htmlFor="companyName">Company Name</label>
	                        <input type="text" className="form-control" name="companyName" value={loanUser.companyName} onChange={this.handleChange} />
	                        {submitted && !loanUser.companyName &&
	                            <div className="help-block">Company Name is required</div>
	                        }
	                    </div>
						<div className={'form-group' + (submitted && !loanUser.designation ? ' has-error' : '')}>
	                        <label className="font-italic" htmlFor="designation">Designation</label>
	                        <input type="text" className="form-control" name="designation" value={loanUser.designation} onChange={this.handleChange} />
	                        {submitted && !loanUser.designation &&
	                            <div className="help-block">Designation is required</div>
	                        }
	                    </div>
						<div className={'form-group' + (submitted && !loanUser.totalExp ? ' has-error' : '')}>
	                        <label className="font-italic" htmlFor="totalExp">Total Exp</label>
	                        <input type="text" className="form-control" name="totalExp" value={loanUser.totalExp} onChange={this.handleChange} />
	                        {submitted && !loanUser.totalExp &&
	                            <div className="help-block">Total Exp is required</div>
	                        }
	                    </div>
						<div className={'form-group' + (submitted && !loanUser.currentExp ? ' has-error' : '')}>
	                        <label className="font-italic" htmlFor="currentExp">Exp with Current company</label>
	                        <input type="text" className="form-control" name="currentExp" value={loanUser.currentExp} onChange={this.handleChange} />
	                        {submitted && !loanUser.currentExp &&
	                            <div className="help-block">Exp with Current company is required</div>
	                        }
	                    </div>
					</div> :
					null
					}
					<div className="form-group">
                        <button className="btn btn-primary">Apply</button>
                        {registering && 
                            <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                        }        
						<Link to="/home" className="btn btn-link">Back to Home Page</Link>
                    </div>
				</form>
				</div>
			</div>
			);
		
		}
}	
	
function mapState(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return { user, users };
}

const actionCreators = {
    getUsers: userActions.getAll,
	logout: userActions.logout
}

const connectedLoanPage = connect(mapState, actionCreators)(LoanPage);
export { connectedLoanPage as LoanPage };