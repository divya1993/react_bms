import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userActions } from '../_actions';

class DepositPage extends React.Component{
	
	componentDidMount() {
        this.props.getUsers();
    }
	 
	constructor(props) {
        super(props);
        this.state = {
            user: {
                accountType: '',
				depositAmount: '',
				availableBalance: '',
				message:'',
				show:false,
				style:''
            },
            submitted: false
        };
		this.handleChange = this.handleChange.bind(this);		
		this.handleAccountTypeChange = this.handleAccountTypeChange.bind(this);
		this.handleDeposit = this.handleDeposit.bind(this);
    }

	handleChange(event) {
        const { name, value } = event.target;
        let user = this.state.user;
        this.setState({
            user: {
                ...user,
                [name]: value,
				
            }
        });
    }
	
	handleAccountTypeChange(event) {
		 const { name, value } = event.target;
        console.log("11name.."+name+" value.."+value)
        const { user } = this.state;
		var availableBalance;
		if(value === 'Savings'){
			console.log("savings")
			availableBalance = 5000
		} else{
			console.log("other")
			availableBalance = 0
		}
        this.setState({
            user: {
                ...user,
				[name]: value,
				availableBalance: availableBalance,
				show: false
            }
        });
    }

	handleDeposit(){
		const user = this.state.user;
		console.log("user.accountType.."+user.accountType+"user.availableBalance.."+user.availableBalance+" user.depositAmount.."+user.depositAmount+".."+(parseInt(user.availableBalance) + parseInt(user.depositAmount)));
		if(user.accountType && user.depositAmount){

		this.setState({
            user: {
                ...user,
				availableBalance: (parseInt(this.state.user.availableBalance) + parseInt(this.state.user.depositAmount)),
				message: 'Deposited successfully!',
				style: 'alert alert-success',
				show: true
            }
        });
	}else{
		this.setState({
            user: {
                ...user,
				message: 'Please enter required details!',
				style: 'alert alert-danger',
				show: true
            },
			submitted: true
	});
	}
	}
	
	render() {
        const { user, submitted } = this.state;
		return (
            <div width="100">
				<div className="shadow-lg p-3 mb-5 bg-white rounded">
				<h1 align="center">Deposit</h1>
				</div>

				&nbsp; 
				{user.show &&
                            <div className={user.style} role="alert">{user.message}</div>
                        }
				&nbsp; 
				<div className="col-md-6 col-md-offset-3">
				<div className={'form-group' + (submitted && !user.accountType ? ' has-error' : '')}>
                        <label className="font-italic" htmlFor="accountType">Account Type</label>
						<div>                        
						<input type="radio" className="form-check-input" name="accountType" value="Savings" onChange={this.handleAccountTypeChange} /> 
						<label className="form-check-label" htmlFor="accountType">
						    Savings
						  </label>
						</div>
						<div>                        
						<input type="radio" className="form-check-input" name="accountType" value="Salary" onChange={this.handleAccountTypeChange} /> 
						<label className="form-check-label" htmlFor="accountType">
						    Salary
						  </label>
						</div>	
                        {submitted && !user.accountType &&
                            <div className="help-block">Account Type is required</div>
                        }
                    </div>
					<div className={'form-group' + (submitted && !user.depositAmount ? ' has-error' : '')}>
                        <label className="font-italic" htmlFor="depositAmount">Deposit Amount</label>
                        <input type="number" className="form-control" name="depositAmount" value={user.depositAmount} onChange={this.handleChange}/>
						{submitted && !user.depositAmount.match(/^[1-9]\d*$/) &&
                            <div className="help-block">Deposit Amount should be greater than 0!</div>
                        }
                        
                    </div>
					<div className="form-group">
                        <button className="btn btn-primary" onClick={this.handleDeposit}>Deposit</button>
                        
                    </div>
					<div className={'form-group' + (submitted && !user.availableBalance ? ' has-error' : '')}>
                        <label className="font-italic" htmlFor="availableBalance">Available Balance (in Rupees)</label>
                        <input type="text" className="form-control" name="availableBalance" value={user.availableBalance} readOnly />
                    </div>
				</div>
				<Link to="/home" className="btn btn-link">Back to Home Page</Link>
			</div>
			);
		
		}
}		


function mapState(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return { user, users };
}

const actionCreators = {
    getUsers: userActions.getAll,
	logout: userActions.logout
}

const connectedDepositPage = connect(mapState, actionCreators)(DepositPage);
export { connectedDepositPage as DepositPage };