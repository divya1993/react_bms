import React from 'react';
import { connect } from 'react-redux';

import { userActions } from '../_actions';

class HomePage extends React.Component {
    componentDidMount() {
        this.props.getUsers();
    }

       render() {
        const { user } = this.props;
		

        return (
            <div >
				<nav className="navbar navbar-expand-lg navbar-dark bg-dark">
				<a className="navbar-brand">
				    <img src="/src/login.jpg" width="30" height="30" alt="" className="rounded-circle"/>
				  </a>

				  <div className="collapse navbar-collapse" id="navbarSupportedContent">
				    <ul className="navbar-nav mr-auto">
				      <li className="nav-item active">
				        <a className="nav-link" href="/home">Home <span className="sr-only">(current)</span></a>
				      </li>
				      <li className="nav-item">
				        <a className="nav-link" href="/loan">Loan</a>
				      </li>
					<li className="nav-item">
				        <a className="nav-link" href="/deposit">Deposit</a>
				      </li>
				      
				    </ul>
				    <form className="form-inline my-2 my-lg-0">
				      <button className="btn btn-outline-success my-2 my-sm-0" type="submit">
						<a className="navbar-brand" href="/login">Logout</a>
						</button>
				    </form>
				  </div>
				</nav>
				  
				&nbsp; 
				<div className="shadow p-3 mb-5 bg-white rounded">
				<h1 className="display-4">Welcome {user.username}!</h1>
				</div>
				&nbsp;
				<h5>Convenient banking solutions. Anytime.. Anywhere..</h5>
				
            </div>
        );
    }
}

function mapState(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return { user, users };
}

const actionCreators = {
    getUsers: userActions.getAll,
	logout: userActions.logout
}

const connectedHomePage = connect(mapState, actionCreators)(HomePage);
export { connectedHomePage as HomePage };