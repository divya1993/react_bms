import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import DatePicker from 'react-datepicker';
import { CountryDropdown, RegionDropdown } from 'react-country-region-selector';
import 'react-datepicker/dist/react-datepicker-cssmodules.css';
import { userActions } from '../_actions';

class RegisterPage extends React.Component {
    constructor(props) {
        super(props);
		
        this.state = {
            user: {
                name: '',
                username: '',
                password: '',
				address: '',
				state: '',
				country: '',
				emailAddress: '',				
				contactNo: '',
				dateOfBirth: new Date(),				
				accountType: '',
				branchName: '',				
				initialDepositAmount: '',
				identificationProofType: '',
				identificationDocumentNo: '',
				age: '',
				citizenStatus: ''
				
            },
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleCountryChange = this.handleCountryChange.bind(this);
        this.handleStateChange = this.handleStateChange.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        const { user } = this.state;
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
    }
    
    handleDateChange(date) {
        console.log("date "+date)
        const { user } = this.state;
		//age calculation
		var today = new Date();
    	var age = today.getFullYear() - date.getFullYear();
    	var month = today.getMonth() - date.getMonth();
   		if (month < 0 || (month === 0 && today.getDate() < date.getDate())) {
        age--;
    	}
    	console.log("age "+age)

		//citizen status calculation
		var citizenStatus = 'Normal';
		if(age < 18){
			citizenStatus='Minor';
		}
		if(age > 18 && age <= 60){
			citizenStatus='Normal';
		}
		if(age > 60){
			citizenStatus='Senior';
		}
		console.log("citizenStatus "+citizenStatus)

        this.setState({
            user: {
                ...user,
                dateOfBirth: date,
				age: age,
				citizenStatus: citizenStatus
            }
        });
    }
    
    handleCountryChange (value) {
    const { user } = this.state;
    this.setState({ 
    user: {
                ...user,
                country : value 
                }
    });
  }
  
  handleStateChange (value) {
    const { user } = this.state;
    this.setState({ 
    user: {
                ...user,
                state : value 
                }
    });
  }

    handleSubmit(event) {
        event.preventDefault();

        this.setState({ submitted: true });
        const { user } = this.state;
        if (user.name && user.username && user.password) {
            this.props.register(user);
        }
    }
    
    render() {
        const { registering  } = this.props;
        const { user, submitted } = this.state;
        return (
            <div className="col-md-6 col-md-offset-3">
				<div className="shadow-lg p-3 mb-5 bg-white rounded">
				<h1 align="center">Registration</h1>
				</div>
                <form name="form" onSubmit={this.handleSubmit}>
                    <div className={'form-group' + (submitted && !user.name ? ' has-error' : '')}>
                        <label className="font-italic" htmlFor="name">Name</label>
                        <input type="text" className="form-control" name="name" value={user.name} onChange={this.handleChange} />
                        {submitted && !user.name &&
                            <div className="help-block">Name is required</div>
                        }
                        {
				           submitted && !user.name.match(/^[a-zA-Z ]+$/) &&
								<div className="help-block">Name should have only alphabets and space!</div>				                   				        
						}
                        
                    </div>
                    <div className={'form-group' + (submitted && !user.username ? ' has-error' : '')}>
                        <label className="font-italic" htmlFor="username">Username</label>
                        <input type="text" className="form-control" name="username" value={user.username} onChange={this.handleChange} />
                        {submitted && !user.username &&
                            <div className="help-block">Username is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !user.password ? ' has-error' : '')}>
                        <label className="font-italic" htmlFor="password">Password</label>
                        <input type="password" className="form-control" name="password" value={user.password} onChange={this.handleChange} />
                        {submitted && !user.password &&
                            <div className="help-block">Password is required</div>
                        }
                    </div>
                    
                    <div className={'form-group' + (submitted && !user.address ? ' has-error' : '')}>
                        <label className="font-italic" htmlFor="address">Address</label>
                        <input type="text" className="form-control" name="address" value={user.address} onChange={this.handleChange} />
                        {submitted && !user.address &&
                            <div className="help-block">Address is required</div>
                        }
                    </div>
                    
                    <div className={'form-group' + (submitted && !user.country ? ' has-error' : '')}>
                        <label className="font-italic" htmlFor="country">Country</label>
                        <CountryDropdown name="country" className="custom-select my-1 mr-sm-2" value={user.country}  onChange={this.handleCountryChange} />

                        {submitted && !user.country &&
                            <div className="help-block">Country is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !user.state ? ' has-error' : '')}>
                        <label className="font-italic" htmlFor="state">State</label>
                        <RegionDropdown className="custom-select my-1 mr-sm-2" name="state" country={user.country} value={user.state} onChange={this.handleStateChange} />
                        {submitted && !user.state &&
                            <div className="help-block">State is required</div>
                        }
                    </div>                    
                    <div className={'form-group' + (submitted && !user.emailAddress ? ' has-error' : '')}>
                        <label className="font-italic" htmlFor="emailAddress">Email Address</label>
                        <input type="text" className="form-control" name="emailAddress" value={user.emailAddress} onChange={this.handleChange} />
                        {submitted && !user.emailAddress &&
                            <div className="help-block">Email Address is required</div>
                        }
                        {
                        submitted && !user.emailAddress.match(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/) && 
                         	<div className="help-block">Invalid Email Address!</div>
                         }
                        
                    </div>
                    
                    <div className={'form-group' + (submitted && !user.contactNo ? ' has-error' : '')}>
                        <label className="font-italic" htmlFor="contactNo">Contact No</label>
                        <input type="text" className="form-control" name="contactNo" value={user.contactNo} maxLength="10" onChange={this.handleChange} />
                        {submitted && !user.contactNo &&
                            <div className="help-block">Contact No is required</div>
                        }
                        {
				           submitted && !user.contactNo.match(/\d{10}/) && 
								<div className="help-block">Contact No should be 10 digits!</div>				                   				        
						}
                        
                    </div>
                    <div className={'form-group' + (submitted && !user.dateOfBirth ? ' has-error' : '')}>
                        <label className="font-italic" htmlFor="dateOfBirth">Date of Birth</label> &nbsp; 
                        <DatePicker className="form-control" name="dateOfBirth" selected={user.dateOfBirth} onChange={this.handleDateChange} maxDate={new Date()} />
                        {submitted && !user.dateOfBirth &&
                            <div className="help-block">Date of Birth is required</div>
                        }
                    </div>
                    
                    <div className={'form-group' + (submitted && !user.accountType ? ' has-error' : '')}>

                        <label className="font-italic" htmlFor="accountType">Account Type</label>
						<div>                        
						<input type="radio" className="form-check-input" name="accountType" value="Savings" onChange={this.handleChange} /> 
						<label className="form-check-label" htmlFor="accountType">
						    Savings
						  </label>
						</div>
						<div>                        
						<input type="radio" className="form-check-input" name="accountType" value="Salary" onChange={this.handleChange} /> 
						<label className="form-check-label" htmlFor="accountType">
						    Salary
						  </label>
						</div>	
                        
                        {submitted && !user.accountType &&
                            <div className="help-block">Account Type is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !user.branchName ? ' has-error' : '')}>
                        <label className="font-italic" htmlFor="branchName">Branch Name</label>
                        <input type="text" className="form-control" name="branchName" value={user.branchName} onChange={this.handleChange} />
                        {submitted && !user.branchName &&
                            <div className="help-block">Branch Name is required</div>
                        }
                    </div>
                    
                    <div className={'form-group' + (submitted && !user.initialDepositAmount ? ' has-error' : '')}>
                        <label className="font-italic" htmlFor="initialDepositAmount">Initial Deposit Amount</label>
                        <input type="text" className="form-control" name="initialDepositAmount" value={user.initialDepositAmount} onChange={this.handleChange} />
                        {submitted && !user.initialDepositAmount &&
                            <div className="help-block">Initial Deposit Amount is required</div>
                        }
						{submitted && !user.initialDepositAmount.match(/^[1-9]\d*$/) &&
                            <div className="help-block">Initial Deposit Amount should be greater than 0!</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !user.identificationProofType ? ' has-error' : '')}>
                        <label className="font-italic" htmlFor="identificationProofType">Identification Proof Type</label>
                        <input type="text" className="form-control" name="identificationProofType" value={user.identificationProofType} onChange={this.handleChange} />
                        {submitted && !user.identificationProofType &&
                            <div className="help-block">Identification Proof Type is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !user.identificationDocumentNo ? ' has-error' : '')}>
                        <label className="font-italic" htmlFor="identificationDocumentNo">Identification Document No</label>
                        <input type="text" className="form-control" name="identificationDocumentNo" value={user.identificationDocumentNo} onChange={this.handleChange} />
                        {submitted && !user.identificationDocumentNo &&
                            <div className="help-block">Identification Document No is required</div>
                        }                        
                    </div>
                                       
                    
                    <div className="form-group">
                        <button className="btn btn-primary">Register</button>
                        {registering && 
                            <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                        }
                        <Link to="/login" className="btn btn-link">Cancel</Link>
                    </div>
                </form>
            </div>
        );
    }
}

function mapState(state) {
    const { registering } = state.registration;
    return { registering };
}

const actionCreators = {
    register: userActions.register
}

const connectedRegisterPage = connect(mapState, actionCreators)(RegisterPage);
export { connectedRegisterPage as RegisterPage };