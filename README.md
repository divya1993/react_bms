# react - Bank Management System

Bank Management System case study for react training

# Software Versions
npm: 6.14.7
react: 16.12.0

# To launch the application,
1. Run 'npm start' in the command prompt.
2. Open http://localhost:8080/login in the browser

# Contains the below pages:
- Login page - /login
- Registration page - /register
- Home page - /home
- Loan page - /loan
- Deposit page - /deposit
 
# Steps to execute
1. If new user, click Register button and create user, else directly enter login details.
2. Once you have logged in, you can view the home page and find the navigation buttons for loan and deposit page.
3. From loan and deposit page, you can navigate back to Home page to Logout.